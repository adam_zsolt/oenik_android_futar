package com.nik.oe.trackingservice.models

data class Location(var lat : Double, var lon : Double) {
}