package com.nik.oe.trackingservice

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.nik.oe.trackingservice.R
import com.nik.oe.trackingservice.models.Person
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var user = intent.extras.getParcelable<Person>("user")

        text.text = "Hello " + user.name + "!"
    }
}
