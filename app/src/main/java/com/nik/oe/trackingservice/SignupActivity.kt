package com.nik.oe.trackingservice

import android.app.Activity
import android.app.DownloadManager
import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

import butterknife.ButterKnife
import butterknife.InjectView
import com.nik.oe.trackingservice.R

import com.nik.oe.trackingservice.R.id.input_username
import com.nik.oe.trackingservice.R.id.input_password
import kotlinx.android.synthetic.main.activity_signup.*
import java.io.File
import java.io.FileOutputStream
import android.os.AsyncTask.execute
import com.squareup.okhttp.*
import java.io.IOException


class SignupActivity : AppCompatActivity() {

    val JSON = MediaType.parse("application/json; charset=utf-8")

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        ButterKnife.inject(this)

        btn_signup!!.setOnClickListener { signup() }

        link_login!!.setOnClickListener {
            // Finish the registration screen and return to the Login activity
            finish()
        }
    }

    fun signup() {
        var success = false
        Log.d(TAG, "Signup")

        if (!validate()) {
            onSignupFailed()
            return
        }

        btn_signup!!.isEnabled = false

        val progressDialog = ProgressDialog(this@SignupActivity,
                R.style.Base_Theme_AppCompat_Dialog)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Creating Account...")
        progressDialog.show()

        val name = input_name!!.text.toString()
        val email = input_email_signup!!.text.toString()
        val password = input_password_signup!!.text.toString()

        var client = OkHttpClient()
        var url = "http://baloghp.cpanel.webgalaxy.hu/xamarinMap/dataprocess.php"
        var json = "{\"Type\":\"Registration\",\"fullname\":\"$name\",\"tel\":\"024243\",\"email\":\"$email\",\"username\":\"$name\",\"password\":\"$password\"}"
        val body = RequestBody.create(JSON, json)
        val request = Request.Builder()
                .url(url)
                .post(body)
                .build()
        client.newCall(request).enqueue(object: Callback{
            override fun onFailure(request: Request?, e: IOException?) {
                e?.printStackTrace()
            }

            override fun onResponse(response: Response?) {
                if(response?.isSuccessful()!!){
                    val myresponse = response.body().string()
                    Log.d("msg: ", myresponse)

                    if(!myresponse.contains("Hiba")){
                        success = true
                    }
                }
            }
        })


        android.os.Handler().postDelayed(
                {
                    // On complete call either onSignupSuccess or onSignupFailed
                    // depending on success
                    if(success){
                        onSignupSuccess()
                    }
                    else {
                        onSignupFailed()
                    }
                    progressDialog.dismiss()
                }, 3000)
    }

    fun onSignupSuccess() {
        Toast.makeText(baseContext, "Sikeres regisztráció!", Toast.LENGTH_LONG).show()
        btn_signup!!.isEnabled = true
        setResult(Activity.RESULT_OK, null)
        finish()
    }

    fun onSignupFailed() {
        Toast.makeText(baseContext, "Sikertelen regisztráció!", Toast.LENGTH_LONG).show()

        btn_signup!!.isEnabled = true
    }

    fun validate(): Boolean {
        var valid = true

        val name = input_name!!.text.toString()
        val email = input_email_signup!!.text.toString()
        val password = input_password_signup!!.text.toString()

        if (name.isEmpty() || name.length < 3) {
            input_name!!.error = "legalább 3 karakter"
            valid = false
        } else {
            input_name!!.error = null
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            input_email_signup!!.error = "adjon meg valós e-mail címet"
            valid = false
        } else {
            input_email_signup!!.error = null
        }

        if (password.isEmpty() || password.length < 4 || password.length > 10) {
            input_password_signup!!.error = "4-10 karakter"
            valid = false
        } else {
            input_email_signup!!.error = null
        }

        return valid
    }

    companion object {
        private val TAG = "SignupActivity"
    }
}