package com.nik.oe.trackingservice.models

import android.R.id.edit
import android.content.Context
import android.preference.PreferenceManager
import android.content.SharedPreferences


class SaveSharedPreferences {
    companion object {
        private val PREF_USER_NAME = "username"
        private val PREF_USER_ID = "user_id"

        private fun getSharedPreferences(ctx: Context): SharedPreferences {
            return PreferenceManager.getDefaultSharedPreferences(ctx)
        }

        fun clearUserName(ctx: Context) {
            val editor = getSharedPreferences(ctx).edit()
            editor.clear() //clear all stored data
            editor.commit()
        }

        fun setUserName(ctx: Context, userName: String) {
            val editor = getSharedPreferences(ctx).edit()
            editor.putString(PREF_USER_NAME, userName)
            editor.commit()
        }

        fun getUserName(ctx: Context): String? {
            return getSharedPreferences(ctx).getString(PREF_USER_NAME, "")
        }

        fun setUserId(ctx: Context, userId: Int){
            val editor = getSharedPreferences(ctx).edit()
            editor.putInt(PREF_USER_ID, userId)
            editor.commit()
        }

        fun getUserId(ctx: Context) : Int? {
            return getSharedPreferences(ctx).getInt(PREF_USER_ID, 0)
        }
    }
}