package com.nik.oe.trackingservice.models

data class Car(val license : String) {
    var brand : String = ""
    var model : String = ""
    var color : String = ""
    var driver : Person? = null
    var free : Boolean = true
    var location : Location? = null
}