package com.nik.oe.trackingservice.models

import java.util.*

data class Route(val start: String, val destination : String) {
    var price : Int = 0
    var registry_date : Date? = null
    var delivery_car : Car? = null
    var status : Int = 0 // 0 : unassigned, 1 : in progress, 2 : finished
}