package com.nik.oe.trackingservice

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import butterknife.ButterKnife
import com.squareup.okhttp.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*
import org.json.JSONArray
import java.io.IOException
import com.nik.oe.trackingservice.models.SaveSharedPreferences



public class LoginActivity : AppCompatActivity() {

    var user = com.nik.oe.trackingservice.models.Person("guest")

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.inject(this)



        btn_login!!.setOnClickListener { login() }

        link_signup!!.setOnClickListener {
            // Start the Signup activity
            val intent = Intent(applicationContext, SignupActivity::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
        }
    }

    fun login() {
        Log.d(TAG, "Login")
        var success = false

//        if (!validate()) {
//            onLoginFailed()
//            return
//        }

        btn_login!!.isEnabled = false

        val progressDialog = ProgressDialog(this@LoginActivity,
                R.style.Base_Theme_AppCompat_Dialog)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Azonosítás...")
        progressDialog.show()

        val username = input_username!!.text.toString()
        val password = input_password!!.text.toString()

        val JSON = MediaType.parse("application/json; charset=utf-8")
        val client = OkHttpClient()
        var url = "http://baloghp.cpanel.webgalaxy.hu/xamarinMap/dataprocess.php"
        var json = "{\"Type\":\"Login\",\"username\":\"$username\",\"password\":\"$password\"}"
        val body = RequestBody.create(JSON, json)
        val request = Request.Builder()
                .url(url)
                .post(body)
                .build()
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(request: Request?, e: IOException?) {
                e?.printStackTrace()
            }

            override fun onResponse(response: Response?) {
                if(response?.isSuccessful!!){
                    val myresponse = response.body().string()
                    Log.d("msg: ", myresponse)

                    if(!myresponse.contains("Felhasználónév vagy a jelszó nem egyezik!")){
                        success = true
                        val reader = JSONArray(myresponse)
                        user = com.nik.oe.trackingservice.models.Person(reader.getJSONObject(0).getString("fullname"))
                        user.id = reader.getJSONObject(0).getString("id").toInt()
                    }
                }
            }
        })

        android.os.Handler().postDelayed(
                {
                    // On complete call either onLoginSuccess or onLoginFailed
                    if(success){
                        onLoginSuccess()
                    }
                    else {
                        onLoginFailed()
                    }
                    progressDialog.dismiss()
                }, 2000)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "SUCCESS", Toast.LENGTH_LONG).show()
                SaveSharedPreferences.setUserName(this, user.username)
                SaveSharedPreferences.setUserId(this, user.id)
            }
        }
    }

    override fun onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true)
    }

    fun onLoginSuccess() {
        btn_login!!.isEnabled = true
        SaveSharedPreferences.setUserName(this, user.name)
        SaveSharedPreferences.setUserId(this, user.id)
        val intent = Intent(applicationContext, MapsActivity::class.java)
        intent.putExtra("user", user)
        startActivity(intent)
        Toast.makeText(this, "Sikeres bejelentkezés!", Toast.LENGTH_SHORT).show()
        finish()
    }

    fun onLoginFailed() {
        Toast.makeText(baseContext, "Sikertelen bejelentkezés!", Toast.LENGTH_LONG).show()

        btn_login!!.isEnabled = true
    }

    fun validate(): Boolean {
        var valid = true

        val email = input_email_signup!!.text.toString()
        val password = input_email_signup!!.text.toString()

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            input_email_signup!!.error = "érvényes e-mail címet adjon meg"
            valid = false
        } else {
            input_email_signup!!.error = null
        }

        if (password.isEmpty() || password.length < 4 || password.length > 10) {
            input_password_signup!!.error = "4-10 karakter"
            valid = false
        } else {
            input_password_signup!!.error = null
        }

        return valid
    }

    companion object {
        private val TAG = "LoginActivity"
        private val REQUEST_SIGNUP = 0
    }
}