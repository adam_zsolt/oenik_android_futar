package com.nik.oe.trackingservice

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.nik.oe.trackingservice.models.Person
import com.nik.oe.trackingservice.models.SaveSharedPreferences
import com.squareup.okhttp.*
import java.io.IOException
import android.view.MenuInflater
import android.view.MenuItem
import com.google.android.gms.maps.model.MarkerOptions


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private var mPermissionDenied = false
    var user = 0

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // action with ID action_refresh was selected
            R.id.logout -> {
                SaveSharedPreferences.clearUserName(this)
                val intent = Intent(applicationContext, LoginActivity::class.java)
                startActivity(intent)
            }
            else -> {
            }
        }

        return true
    }

    override fun onMyLocationButtonClick(): Boolean {
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false
    }

    override fun onMyLocationClick(location: Location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    private fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1001)
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.isMyLocationEnabled = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode != 1001) {
            return
        }

        when (requestCode) {
            1001 -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay!
                    enableMyLocation()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    mPermissionDenied = true
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests
            }
        }
    }

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //SaveSharedPreferences.clearUserName(this)
        if (SaveSharedPreferences.getUserName(this)?.length == 0) {
            // call Login Activity
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        } else {
            // Stay at the current activity.
            user = SaveSharedPreferences.getUserId(this)!!
            setContentView(R.layout.activity_maps)
            //user = intent.extras.getParcelable<Person>("user")
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            val mapFragment = supportFragmentManager
                    .findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getMapAsync(this)
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        var locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10f, locationListener)
        mMap = googleMap
        mMap.setOnMyLocationButtonClickListener(this)
        mMap.setOnMyLocationClickListener(this)

        val pos = LatLng(locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)!!.latitude, locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)!!.longitude)
        mMap.addMarker(MarkerOptions().position(LatLng(47.560133, 19.062181)).title("Ákos"))
        mMap.addMarker(MarkerOptions().position(LatLng(47.599966, 18.951631)).title("Zsolt"))
        mMap.addMarker(MarkerOptions().position(LatLng(47.502645, 19.110246)).title("Ádám"))
        mMap.addMarker(MarkerOptions().position(LatLng(47.521196, 19.039522)).title("Peti"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pos))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 15f))
        enableMyLocation()
    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            // Toast.makeText(this, "" + location.longitude + ":" + location.latitude, Toast.LENGTH_SHORT)
            val JSON = MediaType.parse("application/json; charset=utf-8")
            var success = false
            var client = OkHttpClient()
            var url = "http://baloghp.cpanel.webgalaxy.hu/xamarinMap/dataprocess.php"
            var json = "{\"Type\":\"Location\",\"Lat\":\"${location.latitude}\",\"Lon\":\"${location.longitude}\",\"User\": \"$user\"}"
            val body = RequestBody.create(JSON, json)
            val request = Request.Builder()
                    .url(url)
                    .post(body)
                    .build()
            client.newCall(request).enqueue(object: Callback {
                override fun onFailure(request: Request?, e: IOException?) {
                    e?.printStackTrace()
                }

                override fun onResponse(response: Response?) {
                    if(response?.isSuccessful()!!){
                        val myresponse = response.body().string()
                        Log.d("msg: ", myresponse)

                        if(!myresponse.contains("Hiba")){
                            success = true
                        }
                    }
                }
            })
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError()
            mPermissionDenied = false
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private fun showMissingPermissionError() {
        Toast.makeText(this, "Szükséges engedély hiányzik!", Toast.LENGTH_SHORT).show()
    }
}
