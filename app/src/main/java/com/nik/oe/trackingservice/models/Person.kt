package com.nik.oe.trackingservice.models

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class Person(val name : String) : Parcelable {
    var address : String = ""
    var birthday : Date? = null
    var phone_number : String = ""
    var email : String = ""
    var username : String = ""
    var password : String = ""
    var id : Int = 0

    constructor(parcel: Parcel) : this(parcel.readString()) {
        address = parcel.readString()
        phone_number = parcel.readString()
        email = parcel.readString()
        username = parcel.readString()
        password = parcel.readString()
        id = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeString(phone_number)
        parcel.writeString(email)
        parcel.writeString(username)
        parcel.writeString(password)
        parcel.writeInt(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Person> {
        override fun createFromParcel(parcel: Parcel): Person {
            return Person(parcel)
        }

        override fun newArray(size: Int): Array<Person?> {
            return arrayOfNulls(size)
        }
    }
}